export const MOCK_ALBUMS = [{
    "id": "Gn9jukODs",
    "title": "Touch",
    "artist": "Eurythmics",
    "releaseYear": "1984",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/eurythmics-touch-900-15cccb6a-78ae-4ffe-beaf-1a24dfc3a9df.jpg"
  },
  {
    "id": "n3CQRRjb_",
    "title": "Destroyer",
    "artist": "Kiss",
    "releaseYear": "1976",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/kiss_destroy_coverar_4000dpi300rgb1000147352-d045b89a-36f1-406a-b737-a290887c6e16.jpg?w=1280"
  },
  {
    "id": "cV80drCpx",
    "title": "That's the Way of the World",
    "artist": "Earth, Wind and Fire",
    "releaseYear": "1975",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/51gf-gwqjcl-1-7dd90fad-40da-488d-99f2-494085a9e2b3.jpg?w=500"
  },
  {
    "id": "i603hl5R0",
    "title": "The Smiths",
    "artist": "The Smiths",
    "releaseYear": "1984",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/the_smiths_-_1984_the_smiths_joe_dallesandro-e0440f23-a933-4208-b67f-75d6f22b4e54.jpg?w=1280"
  },
  {
    "id": "Wqc2CwhCl",
    "title": "A Rush of Blood to the Head",
    "artist": "Coldplay",
    "releaseYear": "2002",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/coldplay-a-rush-of-blood-to-the-head-b994b5e3-9684-43b7-af44-f0a23154c100.jpg?w=950"
  },
  {
    "id": "CISkEnA1N",
    "title": "Hysteria",
    "artist": "Def Leppard",
    "releaseYear": "1987",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/8560698_f1024-a994fb27-87cf-46f0-9d8e-aba23729aa1f.jpg?w=1024"
  },
  {
    "id": "PoyIVmXlq",
    "title": "Boys Don't Cry",
    "artist": "The Cure",
    "releaseYear": "1980",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/rs-136702-2ee61387fb50efc38b003d13cae18374e4b0157b.jpg?w=306"
  },
  {
    "id": "ahTtrRacc",
    "title": "Sandinista",
    "artist": "The Clash",
    "releaseYear": "1980",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/rs-136731-ca9a65d59894fe510ca5f05d6fad033850da36b7.jpg?w=306"
  },
  {
    "id": "KLUsKzPRd",
    "title": "Californication",
    "artist": "Red Hot Chili Peppers",
    "releaseYear": "1999",
    "albumArt": "https://www.rollingstone.com/wp-content/uploads/2018/06/rs-136736-815e81c0c12a2447982ec2164f3f86872b15415c.jpg?w=306"
  }];